package com.rocketbuild.android.sandbox;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public final ActivityRule<MainActivity> main = new ActivityRule<>(MainActivity.class);


    @Test
    public void shouldBeAbleToLaunchMainScreen() {
        onView(withText("Hello")).check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void testEnterName() {
        onView(withId(R.id.editText)).perform(typeText("John"));
        onView(withId(R.id.button)).perform(click());
        onView(withText("John")).check(ViewAssertions.matches(isDisplayed()));
    }

}
